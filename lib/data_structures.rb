# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  # your code goes here
  arr.max - arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  # your code goes here
  arr.sort == arr
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  # your code goes here
  vowels = %w( a e i o u A I E O U)
  count = 0
  str.chars.each do |i|

    if vowels.include?(i)
      count += 1
    end

  end
return count
end



# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # your code goes here
  str.gsub(/[aeiouAIEOU]/, '')
end



# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  # your code goes here
  int.to_s.split("").sort.reverse
end


# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  # your code goes here
  str.downcase.split('').uniq != str.downcase.split('')
end


# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  # your code goes here
  arr_new = arr.join
 "(#{arr_new[0..2]}) #{arr_new[3..5]}-#{arr_new[6..9]}"
end



# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  # your code goes here
  str.split(",").sort.max.to_i  - str.split(",").sort.min.to_i
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset= 1)
  # this code could be better 
  pos_offset = 0
  i = 0
  array_new = arr
  if offset < 0

    pos_offset = arr.length + (offset)
    while i < pos_offset
      array_new = arr.push(arr.shift)
      i += 1
    end
  end

  while i < offset
    array_new = arr.push(arr.shift)
    i += 1
  end
  array_new
end
# array1= [1,2,3,4,5]
# p my_rotate(array1, offset= -1)
